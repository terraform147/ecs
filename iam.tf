# Cluster Execution Role
resource "aws_iam_role" "ecs_execution_role" {
  name               = "${var.name}-ecs-task-role"
  assume_role_policy = file("${path.module}/policies/ecs-task-execution-role.json")
}

# Cluster Execution Policy
resource "aws_iam_role_policy" "ecs_execution_role_policy" {
  name   = "${var.name}-role-policy"
  policy = file("${path.module}/policies/ecs-execution-role-policy.json")
  role   = aws_iam_role.ecs_execution_role.id
}

data "template_file" "ssm_policy" {
  template = file("${path.module}/policies/ecs-ssm-role-policy.json")

  vars = {
    ssm_path        = "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.id}:parameter/${var.name}/general/*"
    kms_key_arn     = "arn:aws:kms:${var.region}:${data.aws_caller_identity.current.id}:key/${data.aws_kms_key.selected.id}"
  }
}

# Cluster SSM Policy
resource "aws_iam_role_policy" "ecs_ssm_role_policy" {
  name   = "${var.name}-ssm-role-policy"
  policy = data.template_file.ssm_policy.rendered
  role   = aws_iam_role.ecs_execution_role.id
}
